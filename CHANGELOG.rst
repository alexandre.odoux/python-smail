=======
CHANGES
=======

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

0.x.0 (unreleased)
------------------

- ...

0.4.0 (2020-03-16)
------------------

- add: sign_and_encrypt

0.3.0 (2020-03-14)
------------------

- remove: six

0.2.2 (2020-03-14)
------------------

- add: implement automated tests
- add: encrypt
- add: sign

0.1.0 (2020-03-14)
------------------

- Initial release.
